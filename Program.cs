﻿// See https://aka.ms/new-console-template for more information

namespace UniversityLaboratory2._5
{
    class Program
    {
        private static void Main()
        {
            ConsoleMessageGenerator.GenerateLaboratoryTitle(
                new LaboratoryInfo(
                    "Бродский Егор Станиславович",
                    "ИР-133Б",
                    5,
                    "Одновимірні масиви."
                )
            );
            Console.WriteLine();
            TaskChooser.Run(new TaskInfo[]
            {
                new("Завдання 1", () =>
                {
                    Console.WriteLine("Введіть величину масиву:");
                    var size = Utils.ToInt(Console.ReadLine());
                    
                    var array = Utils.GenerateRandomArray(size, -100, 100);
                    var max = array.Max();
                    var maxPos = Array.IndexOf(array, max);


                    var negativeNumbersCount = 0;
                    for (int i = 0; i < maxPos + 1; i++)
                    {
                        if (array[i] < 0)
                        {
                            negativeNumbersCount++;
                        }
                    }
                    
                    Console.WriteLine("Введений масив:");
                    Console.WriteLine(string.Join(", ", array));
                    Console.WriteLine($"Найбільше додатне число в масиві: {max}");
                    Console.WriteLine($"Позиція найбільшого додатного числа в масиві: {maxPos}");
                    Console.WriteLine($"Кількість від'ємних чисел в масиві: {negativeNumbersCount}");
                    return null;
                }),
            });
        }
    }
}